import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-compra',
  templateUrl: './compra.component.html',
  styleUrls: ['./compra.component.css']
})
export class CompraComponent implements OnInit {

  usuario = "Vanessa";
  frecuencia = "Frecuencia"

  modRef(valor: string) {
    this.frecuencia = valor
  }

  constructor() { }

  ngOnInit(): void {
  }
  alerta1(){
    
    Swal.fire({
      icon: 'success',
      title: '¡Éxito!',
      text: 'Tu compra se ha realizado con éxito',
      confirmButtonColor: "#fcaf1a",
    }).then( function(){
      
  Swal.fire({
    title: '¡Felicidades!',
    text:'Has obtenido el logro de comprar 100 artículos de un sólo productor, en agradecimiento obtuviste un obsequio, búscalo en las notificaciones de tu perfil',
    width: 600,
    padding: '3em',
    
    confirmButtonText:'Ir al perfil',
    confirmButtonColor: "#fcaf1a",
    backdrop: `
      rgba(0,0,123,0.4)
      //url("/assets/fireworks.gif")
      left top
      no-repeat
      width:100%
    `,
  }).then(function(){
    window.location.href = "/perfilUsuario";
  }
    
  )
    })
    

}
alerta2(){
}
}
