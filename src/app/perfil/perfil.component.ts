import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  notiAlert(){
    
    
    Swal.fire({
      imageUrl: '/assets/planta.png',
      text: '¡Has llegado a nivel 10 en la compra de tus productos!Pronto un productor te enviará una carta a tu correo agradeciendo el apoyo.',
      confirmButtonColor: "#fcaf1a",
    })
    //alert('¡Has llegado a Nivel 10 en la compra de tus productos! Pronto un productor te enviará una carta de agradecimiento a tu email, agradeciendo el apoyo.')
  }

}
